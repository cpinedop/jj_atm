package com.cpp.chalenge.johnson.atm.model;

/**
 * The Class Credentials.
 */
public class Credentials {

	/** The account number. */
	private Long accountNumber;

	/** The pin number. */
	private Long pinNumber;

	/**
	 * Instantiates a new credentials.
	 *
	 * @param accountNumber the account number
	 * @param pinNumber     the pin number
	 */
	public Credentials(final Long accountNumber, final Long pinNumber) {
		super();
		this.accountNumber = accountNumber;
		this.pinNumber = pinNumber;
	}

	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public Long getAccountNumber() {
		return this.accountNumber;
	}

	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(final Long accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * Gets the pin number.
	 *
	 * @return the pin number
	 */
	public Long getPinNumber() {
		return this.pinNumber;
	}

	/**
	 * Sets the pin number.
	 *
	 * @param pinNumber the new pin number
	 */
	public void setPinNumber(final Long pinNumber) {
		this.pinNumber = pinNumber;
	}

}
