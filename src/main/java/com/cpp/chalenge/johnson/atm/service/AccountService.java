package com.cpp.chalenge.johnson.atm.service;

import com.cpp.chalenge.johnson.atm.model.AccountBalance;
import com.cpp.chalenge.johnson.atm.model.Credentials;

/**
 * The AccountService Interface.
 */
public interface AccountService {

	/**
	 * Gets the account balance.
	 *
	 * Valid credentials are needed
	 *
	 * @param credentials the credentials
	 * @return the account balance
	 */
	AccountBalance getAccountBalance(Credentials credentials);

	/**
	 * Gets the current balance.
	 *
	 * Valid credentials are needed
	 *
	 * @param credentials the credentials
	 * @return the current balance
	 */
	Long getCurrentBalance(Credentials credentials);

	/**
	 * Gets the max withdrawal.
	 *
	 * Valid credentials are needed
	 *
	 * @param credentials the credentials
	 * @return the max withdrawal
	 */
	Long getMaxWithdrawal(Credentials credentials);
}
