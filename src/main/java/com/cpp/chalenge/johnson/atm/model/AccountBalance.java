package com.cpp.chalenge.johnson.atm.model;

/**
 * The Class AccountBalance.
 */
public class AccountBalance {

	/** The balance. */
	private Long balance;

	/** The max withdrawal. */
	private Long maxWithdrawal;

	/** The account number. */
	private Long accountNumber;

	/**
	 * Instantiates a new account balance.
	 */
	public AccountBalance() {
	}

	/**
	 * Instantiates a new account balance.
	 *
	 * @param balance       the balance
	 * @param maxWithdrawal the max withdrawal
	 * @param accountNumber the account number
	 */
	public AccountBalance(final Long balance, final Long maxWithdrawal, final Long accountNumber) {
		super();
		this.balance = balance;
		this.maxWithdrawal = maxWithdrawal;
		this.accountNumber = accountNumber;
	}

	/**
	 * Gets the balance.
	 *
	 * @return the balance
	 */
	public Long getBalance() {
		return this.balance;
	}

	/**
	 * Sets the balance.
	 *
	 * @param balance the new balance
	 */
	public void setBalance(final Long balance) {
		this.balance = balance;
	}

	/**
	 * Gets the max withdrawal.
	 *
	 * @return the max withdrawal
	 */
	public Long getMaxWithdrawal() {
		return this.maxWithdrawal;
	}

	/**
	 * Sets the max withdrawal.
	 *
	 * @param maxWithdrawal the new max withdrawal
	 */
	public void setMaxWithdrawal(final Long maxWithdrawal) {
		this.maxWithdrawal = maxWithdrawal;
	}

	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public Long getAccountNumber() {
		return this.accountNumber;
	}

	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(final Long accountNumber) {
		this.accountNumber = accountNumber;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.accountNumber == null) ? 0 : this.accountNumber.hashCode());
		result = prime * result + ((this.balance == null) ? 0 : this.balance.hashCode());
		result = prime * result + ((this.maxWithdrawal == null) ? 0 : this.maxWithdrawal.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final AccountBalance other = (AccountBalance) obj;
		if (this.accountNumber == null) {
			if (other.accountNumber != null) {
				return false;
			}
		} else if (!this.accountNumber.equals(other.accountNumber)) {
			return false;
		}
		if (this.balance == null) {
			if (other.balance != null) {
				return false;
			}
		} else if (!this.balance.equals(other.balance)) {
			return false;
		}
		if (this.maxWithdrawal == null) {
			if (other.maxWithdrawal != null) {
				return false;
			}
		} else if (!this.maxWithdrawal.equals(other.maxWithdrawal)) {
			return false;
		}
		return true;
	}

}
