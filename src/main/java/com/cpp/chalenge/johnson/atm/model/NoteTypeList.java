package com.cpp.chalenge.johnson.atm.model;

import java.util.ArrayList;

/**
 * The Class NoteTypeList.
 */
public class NoteTypeList extends ArrayList<NoteType> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3321040977145617319L;

	/**
	 * Gets the total value.
	 *
	 * @return the total value
	 */
	public Long getTotalValue() {
		return stream().mapToLong(note -> note.getValue()).sum();
	}
}
