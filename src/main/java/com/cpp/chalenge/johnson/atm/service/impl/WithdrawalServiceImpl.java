package com.cpp.chalenge.johnson.atm.service.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cpp.chalenge.johnson.atm.annotation.Loggable;
import com.cpp.chalenge.johnson.atm.annotation.PinValidation;
import com.cpp.chalenge.johnson.atm.mapper.NoteMapper;
import com.cpp.chalenge.johnson.atm.mapper.WithdrawalMapper;
import com.cpp.chalenge.johnson.atm.model.AccountBalance;
import com.cpp.chalenge.johnson.atm.model.Credentials;
import com.cpp.chalenge.johnson.atm.model.Note;
import com.cpp.chalenge.johnson.atm.model.NoteList;
import com.cpp.chalenge.johnson.atm.model.NoteType;
import com.cpp.chalenge.johnson.atm.model.NoteTypeList;
import com.cpp.chalenge.johnson.atm.model.WithdrawalOperation;
import com.cpp.chalenge.johnson.atm.service.AccountService;
import com.cpp.chalenge.johnson.atm.service.WithdrawalService;

/**
 * The Class WithdrawalServiceImpl.
 */
@Service
public class WithdrawalServiceImpl implements WithdrawalService {

	/** The logger. */
	final Logger logger = LogManager.getLogger(WithdrawalServiceImpl.class);

	/** The note mapper. */
	@Autowired
	private NoteMapper noteMapper;

	/** The withdrawal mapper. */
	@Autowired
	private WithdrawalMapper withdrawalMapper;

	/** The account service. */
	@Autowired
	private AccountService accountService;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.cpp.chalenge.johnson.atm.service.WithdrawalService#
	 * requestWithdrawalOperation(com.cpp.chalenge.johnson.atm.model.Credentials,
	 * java.lang.Long)
	 */
	@Override
	@Loggable
	@PinValidation
	public WithdrawalOperation requestWithdrawalOperation(final Credentials credentials,
			final Long withdrawalRequested) {
		final AccountBalance accountBalance = this.accountService.getAccountBalance(credentials);

		final WithdrawalOperation withdrawalOperation = new WithdrawalOperation();
		withdrawalOperation.setWithdrawalRequested(withdrawalRequested);
		withdrawalOperation.setAccountBalance(accountBalance);

		final Set<Note> availableNotesSet = this.noteMapper.getAvailableNotes();
		final NoteList availableNotes = new NoteList();
		for (final Note noteFromSet : availableNotesSet) {
			availableNotes.add(noteFromSet);
		}

		// withdrawalNotes stores the notes to withdraw, in case that the requested amount doesn't fit the 
		// available notes it will store the nearest below alternative
		final NoteList withdrawalNotes = getNotesForWithdrawal(availableNotes, withdrawalRequested);

		if (withdrawalNotes.getTotalValue().longValue() == withdrawalRequested.longValue()) {
			withdrawalOperation.setInsufficientNotes(false);
			withdrawalOperation.setNotes(withdrawalNotes);
			this.withdrawalMapper.insertWithdrawal(withdrawalOperation);
			for (final Note noteToUpdate : availableNotes) {
				this.noteMapper.updateNotes(noteToUpdate.getType().getId(), noteToUpdate.getAmount());
			}
			// Reload balance
			withdrawalOperation.setAccountBalance(this.accountService.getAccountBalance(credentials));
		} else {
			withdrawalOperation.setInsufficientNotes(true);
			final List<NoteList> alternatives = new ArrayList<>();
			alternatives.add(withdrawalNotes);
			addUpperAlternativeIfPossible(accountBalance, availableNotes, withdrawalNotes, alternatives);
			withdrawalOperation.setAlternatives(alternatives);
		}

		return withdrawalOperation;
	}

	/**
	 * Adds the upper alternative if possible.
	 *
	 * @param balance         the balance
	 * @param availableNotes  the available notes
	 * @param belowAlternativeNotes the withdrawal notes
	 * @param alternatives    the alternatives
	 */
	private void addUpperAlternativeIfPossible(final AccountBalance balance, NoteList availableNotes,
			final NoteList belowAlternativeNotes, final List<NoteList> alternatives) {
		// Retrieves the smallest available note after subtracting the notes needed to
		// get the below alternative amount
		final Optional<Note> smallestAvailableNote = availableNotes.stream().sorted(Comparator.reverseOrder())
				.findFirst();
		if (smallestAvailableNote.isPresent()) {
			final Long upperAlternativeValue = belowAlternativeNotes.getTotalValue()
					+ smallestAvailableNote.get().getType().getValue();
			// If the account has enough founds
			if (upperAlternativeValue <= balance.getMaxWithdrawal()) {
				// Refresh the available notes
				final Set<Note> availableNotesSet = this.noteMapper.getAvailableNotes();
				availableNotes = new NoteList();
				for (final Note noteFromSet : availableNotesSet) {
					availableNotes.add(noteFromSet);
				}
				// Calculate better note set again
				final NoteList upperAlternativeNotes = getNotesForWithdrawal(availableNotes, upperAlternativeValue);
				// Add it to alternatives
				alternatives.add(upperAlternativeNotes);
			}
		}
	}

	/**
	 * Gets the notes for withdrawal.
	 *
	 * Calculates the minimum notes to fit the requested amount. It will use only
	 * the available notes.
	 *
	 * @param availableNotes      the available notes
	 * @param withdrawalRequested the withdrawal requested
	 * @return the notes for withdrawal
	 */
	private NoteList getNotesForWithdrawal(final NoteList availableNotes, final Long withdrawalRequested) {
		final NoteTypeList notesArray = new NoteTypeList();
		getNotesForWithdrawalInner(notesArray, availableNotes, withdrawalRequested);
		return new NoteList(notesArray);
	}

	/**
	 * Auxiliary function for getNotesForWithdrawal.
	 *
	 * @param result              the result
	 * @param availableNotes      the available notes
	 * @param withdrawalRequested the withdrawal requested
	 * @return the notes for withdrawal inner
	 */
	private void getNotesForWithdrawalInner(final List<NoteType> result, final Set<Note> availableNotes,
			final Long withdrawalRequested) {
		// Gets the largest note among the available ones that can be used to reach the
		// right amount.
		availableNotes.stream().sorted()
				.filter(note -> note.getType().getValue() <= withdrawalRequested && note.getAmount() > 0).findFirst()
				.ifPresent(note -> {
					result.add(note.getType());
					note.setAmount(note.getAmount() - 1);
					// Adds the note to the result, subtract the amount and call himself
					getNotesForWithdrawalInner(result, availableNotes, withdrawalRequested - note.getType().getValue());
				});
	}

}
