package com.cpp.chalenge.johnson.atm.security.exceptions;

import org.springframework.security.core.AuthenticationException;

/**
 * The Class AuthenticationATMException.
 */
public class AuthenticationATMException extends AuthenticationException {

	/**
	 * Instantiates a new authentication ATM exception.
	 *
	 * @param msg the message
	 */
	public AuthenticationATMException(final String msg) {
		super(msg);
	}

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2393665154569734694L;

}
