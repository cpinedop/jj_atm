package com.cpp.chalenge.johnson.atm.application.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ch.vorburger.exec.ManagedProcessException;
import ch.vorburger.mariadb4j.DBConfigurationBuilder;
import ch.vorburger.mariadb4j.springframework.MariaDB4jSpringService;

/**
 * The Class EmbeddedDBConfig.
 *
 * Start a MariaDB4j with the web server
 *
 * @author kernelPanic
 */
@Configuration
public class EmbeddedDBConfig {

	/**
	 * Maria DB 4 j spring service.
	 *
	 * @return the maria DB 4 j spring service
	 */
	@Bean
	MariaDB4jSpringService mariaDB4jSpringService() {
		return new MariaDB4jSpringService();
	}

	/**
	 * Data source.
	 *
	 * @param mariaDB4jSpringService the maria DB 4 j spring service
	 * @param databaseName           the database name
	 * @param datasourceUsername     the datasource username
	 * @param datasourcePassword     the datasource password
	 * @param datasourceDriver       the datasource driver
	 * @return the data source
	 */
	@Bean
	DataSource dataSource(final MariaDB4jSpringService mariaDB4jSpringService,
			@Value("${app.mariaDB4j.databaseName}") final String databaseName,
			@Value("${spring.datasource.username}") final String datasourceUsername,
			@Value("${spring.datasource.password}") final String datasourcePassword,
			@Value("${spring.datasource.driver-class-name}") final String datasourceDriver,
			@Value("${app.mariaDB4j.sql-resources}") final String sqlResources) throws ManagedProcessException {
		// Create our database with default root user and no password
		mariaDB4jSpringService.getDB().createDB(databaseName);
		mariaDB4jSpringService.getDB().source(sqlResources);

		final DBConfigurationBuilder config = mariaDB4jSpringService.getConfiguration();

		return DataSourceBuilder.create().username(datasourceUsername).password(datasourcePassword)
				.url(config.getURL(databaseName)).driverClassName(datasourceDriver).build();
	}
}
