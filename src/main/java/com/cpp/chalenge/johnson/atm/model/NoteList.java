package com.cpp.chalenge.johnson.atm.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The Class NoteList.
 */
public class NoteList extends HashSet<Note> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2456565688823992725L;

	/**
	 * Instantiates a new note list.
	 *
	 * @param notesArray the notes array
	 */
	public NoteList(final NoteTypeList notesArray) {
		super();
		final Map<NoteType, Note> notesMap = groupNotesByType(notesArray);
		notesMap.values().stream().forEach(note -> add(note));
	}

	/**
	 * Instantiates a new note list.
	 *
	 * @param availableNotesSet the available notes set
	 */
	public NoteList(final Set<Note> availableNotesSet) {
		addAll(availableNotesSet);
	}

	/**
	 * Instantiates a new note list.
	 */
	public NoteList() {
		super();
	}

	/**
	 * Group notes by type.
	 *
	 * @param notesArray the notes array
	 * @return the map
	 */
	private Map<NoteType, Note> groupNotesByType(final List<NoteType> notesArray) {
		final Map<NoteType, Note> notesMap = new HashMap<>();
		notesArray.stream().forEach(noteType -> {
			if (notesMap.get(noteType) != null) {
				notesMap.get(noteType).setAmount(notesMap.get(noteType).getAmount() + 1);
			} else {
				notesMap.put(noteType, new Note(noteType, 1L));
			}
		});
		return notesMap;
	}

	/**
	 * Gets the total value.
	 *
	 * @return the total value
	 */
	public Long getTotalValue() {
		return stream().mapToLong(note -> note.getType().getValue() * note.getAmount()).sum();
	}

}
