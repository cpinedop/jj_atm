package com.cpp.chalenge.johnson.atm.service;

import com.cpp.chalenge.johnson.atm.model.Credentials;
import com.cpp.chalenge.johnson.atm.model.WithdrawalOperation;

/**
 * The Interface WithdrawalService.
 */
public interface WithdrawalService {

	/**
	 * Request withdrawal operation.
	 *
	 * This method will return one of this two options:
	 *
	 * If the withdrawal is possible, the method will return the account balance after the
	 * withdrawal and the extracted notes.
	 *
	 * If the withdrawal is NOT possible, the method will return the current account balance and
	 * two possibilities (above and below quantity closest to the requested amount) available to withdraw.
	 *
	 * @param credentials         the credentials
	 * @param withdrawalRequested the withdrawal requested
	 * @return the withdrawal operation
	 */
	WithdrawalOperation requestWithdrawalOperation(Credentials credentials, Long withdrawalRequested);

}
