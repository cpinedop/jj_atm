package com.cpp.chalenge.johnson.atm.security;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.cpp.chalenge.johnson.atm.security.exceptions.AuthenticationATMException;

/**
 * The Class SecureCardAuthenticationProvider.
 *
 * Uses the SecureCardTokenAuthentication to store the user account. The account
 * must be sent in the header of every request.
 */
@Component
public class SecureCardAuthenticationProvider implements AuthenticationProvider {

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.security.authentication.AuthenticationProvider#
	 * authenticate(org.springframework.security.core.Authentication)
	 */
	@Override
	public Authentication authenticate(final Authentication authentication) {
		final SecureCardTokenAuthentication scta = (SecureCardTokenAuthentication) authentication;

		// Verify the existence of the account
		if (scta == null || scta.getPrincipal() == null) {
			throw new AuthenticationATMException("Invalid card");
		}

		return scta;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.springframework.security.authentication.AuthenticationProvider#supports(
	 * java.lang.Class)
	 */
	@Override
	public boolean supports(final Class<?> authentication) {
		return SecureCardTokenAuthentication.class.isAssignableFrom(authentication);
	}

}
