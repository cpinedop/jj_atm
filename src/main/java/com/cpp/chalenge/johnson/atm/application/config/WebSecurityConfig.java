package com.cpp.chalenge.johnson.atm.application.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.filter.OncePerRequestFilter;

import com.cpp.chalenge.johnson.atm.security.SecureCardAuthFilter;
import com.cpp.chalenge.johnson.atm.security.SecureCardAuthenticationProvider;

/**
 * The Class WebSecurityConfig.
 *
 * Configures the security for every request to the application. The requests
 * must contain the accountNumber in the accountNumber header or will be
 * forbidden
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	/** The auth provider. */
	@Autowired
	private SecureCardAuthenticationProvider authProvider;

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.security.config.annotation.web.configuration.
	 * WebSecurityConfigurerAdapter#configure(org.springframework.security.config.
	 * annotation.web.builders.HttpSecurity)
	 */
	@Override
	protected void configure(final HttpSecurity http) throws Exception {
		http.antMatcher("/**").csrf().disable().headers().frameOptions().disable().and().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests().anyRequest()
				.authenticated().and().addFilterBefore(authCheckFilter(), BasicAuthenticationFilter.class);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.security.config.annotation.web.configuration.
	 * WebSecurityConfigurerAdapter#configure(org.springframework.security.config.
	 * annotation.authentication.builders.AuthenticationManagerBuilder)
	 */
	@Override
	protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(this.authProvider);
	}

	/**
	 * Auth check filter.
	 *
	 * @return the once per request filter
	 */
	@Bean
	public OncePerRequestFilter authCheckFilter() {
		return new SecureCardAuthFilter();
	}
}
