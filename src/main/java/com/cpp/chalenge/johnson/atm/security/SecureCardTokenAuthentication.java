package com.cpp.chalenge.johnson.atm.security;

import java.util.Arrays;
import java.util.Collection;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

/**
 * The Class SecureCardTokenAuthentication.
 *
 * Custom authentication object. It stores the user account as token.
 */
public class SecureCardTokenAuthentication extends AbstractAuthenticationToken {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 977377575531209660L;

	/** The token. */
	private final Long token;

	/**
	 * Constructor
	 *
	 * @param token the token
	 */
	public SecureCardTokenAuthentication(final Long token) {
		super(Arrays.asList());
		this.token = token;
	}

	/**
	 * Default constructor
	 *
	 * @param authorities       the authorities
	 * @param authenticatedUser the authenticated user
	 * @param token             the token
	 */
	public SecureCardTokenAuthentication(final Collection<? extends GrantedAuthority> authorities,
			final Long authenticatedUser, final Long token) {
		super(authorities);
		this.token = token;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.security.core.Authentication#getCredentials()
	 */
	@Override
	public Object getCredentials() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.security.core.Authentication#getPrincipal()
	 */
	@Override
	public Object getPrincipal() {
		return this.token;
	}

}
