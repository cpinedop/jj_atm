package com.cpp.chalenge.johnson.atm.aop;

import java.lang.reflect.Method;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

/**
 * The Class LoggingAspect.
 *
 * Every method annotated with the annotation Loggable will log at trace level
 * the entry and exit of the method.
 */
@Aspect
@Component
public class LoggingAspect {

	/**
	 * Around.
	 *
	 * @param joinPoint the join point
	 * @return the object
	 * @throws Throwable the throwable
	 */
	@Around("@annotation(com.cpp.chalenge.johnson.atm.annotation.Loggable)")
	public Object around(final ProceedingJoinPoint joinPoint) throws Throwable {
		final Logger logger = LogManager.getLogger(joinPoint.getTarget().getClass());

		final MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		final Method method = signature.getMethod();

		logger.trace("Entry " + method.getName());
		final Object result = joinPoint.proceed();
		logger.trace("Exit " + method.getName());
		return result;
	}
}
