package com.cpp.chalenge.johnson.atm.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;

import com.cpp.chalenge.johnson.atm.model.WithdrawalOperation;

/**
 * The Interface WithdrawalMapper.
 */
@Mapper
public interface WithdrawalMapper {

	/**
	 * Insert withdrawal.
	 *
	 * @param withdrawal the withdrawal
	 * @return the long
	 */
	@Insert("INSERT INTO t_withdrawals (account_number, withdrawal_amount) VALUES (#{withdrawal.accountBalance.accountNumber},#{withdrawal.withdrawalRequested});")
	@Options(useGeneratedKeys = true, keyProperty = "withdrawal.withdrawalId", keyColumn = "id_withdrawal")
	void insertWithdrawal(@Param("withdrawal") WithdrawalOperation withdrawal);
}
