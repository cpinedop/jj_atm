package com.cpp.chalenge.johnson.atm.model;

/**
 * The Class NoteType.
 */
public class NoteType {

	/** The id. */
	private Long id;

	/** The value. */
	private Long value;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return this.id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public Long getValue() {
		return this.value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(final Long value) {
		this.value = value;
	}

}
