package com.cpp.chalenge.johnson.atm.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The Class WithdrawalOperation.
 */
@JsonInclude(Include.NON_NULL)
public class WithdrawalOperation {

	/** The withdrawal id. */
	private Long withdrawalId;

	/** The withdrawal requested. */
	private Long withdrawalRequested;

	/** The account balance. */
	private AccountBalance accountBalance;

	/** The notes. */
	private NoteList notes;

	/** The insufficient notes. */
	private boolean insufficientNotes;

	/** The alternatives. */
	private List<NoteList> alternatives;

	/**
	 * Gets the withdrawal id.
	 *
	 * @return the withdrawal id
	 */
	public Long getWithdrawalId() {
		return this.withdrawalId;
	}

	/**
	 * Sets the withdrawal id.
	 *
	 * @param withdrawalId the new withdrawal id
	 */
	public void setWithdrawalId(final Long withdrawalId) {
		this.withdrawalId = withdrawalId;
	}

	/**
	 * Gets the withdrawal requested.
	 *
	 * @return the withdrawal requested
	 */
	public Long getWithdrawalRequested() {
		return this.withdrawalRequested;
	}

	/**
	 * Sets the withdrawal requested.
	 *
	 * @param withdrawalRequested the new withdrawal requested
	 */
	public void setWithdrawalRequested(final Long withdrawalRequested) {
		this.withdrawalRequested = withdrawalRequested;
	}

	/**
	 * Gets the account balance.
	 *
	 * @return the account balance
	 */
	public AccountBalance getAccountBalance() {
		return this.accountBalance;
	}

	/**
	 * Sets the account balance.
	 *
	 * @param accountBalance the new account balance
	 */
	public void setAccountBalance(final AccountBalance accountBalance) {
		this.accountBalance = accountBalance;
	}

	/**
	 * Checks if is insufficient notes.
	 *
	 * @return true, if is insufficient notes
	 */
	public boolean isInsufficientNotes() {
		return this.insufficientNotes;
	}

	/**
	 * Sets the insufficient notes.
	 *
	 * @param insufficientNotes the new insufficient notes
	 */
	public void setInsufficientNotes(final boolean insufficientNotes) {
		this.insufficientNotes = insufficientNotes;
	}

	/**
	 * Gets the notes.
	 *
	 * @return the notes
	 */
	public NoteList getNotes() {
		return this.notes;
	}

	/**
	 * Sets the notes.
	 *
	 * @param notes the new notes
	 */
	public void setNotes(final NoteList notes) {
		this.notes = notes;
	}

	/**
	 * Gets the alternatives.
	 *
	 * @return the alternatives
	 */
	public List<NoteList> getAlternatives() {
		return this.alternatives;
	}

	/**
	 * Sets the alternatives.
	 *
	 * @param alternatives the new alternatives
	 */
	public void setAlternatives(final List<NoteList> alternatives) {
		this.alternatives = alternatives;
	}

}
