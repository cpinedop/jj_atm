package com.cpp.chalenge.johnson.atm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cpp.chalenge.johnson.atm.annotation.PinValidation;
import com.cpp.chalenge.johnson.atm.mapper.AccountMapper;
import com.cpp.chalenge.johnson.atm.model.AccountBalance;
import com.cpp.chalenge.johnson.atm.model.Credentials;
import com.cpp.chalenge.johnson.atm.service.AccountService;

/**
 * The Class AccountServiceImpl.
 */
@Service
public class AccountServiceImpl implements AccountService {

	/** The account mapper. */
	@Autowired
	private AccountMapper accountMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cpp.chalenge.johnson.atm.service.AccountService#getCurrentBalance(com.cpp
	 * .chalenge.johnson.atm.model.Credentials)
	 */
	@Override
	@PinValidation
	public Long getCurrentBalance(final Credentials credentials) {
		return this.accountMapper.getAccountBalance(credentials.getAccountNumber());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cpp.chalenge.johnson.atm.service.AccountService#getMaxWithdrawal(com.cpp.
	 * chalenge.johnson.atm.model.Credentials)
	 */
	@Override
	@PinValidation
	public Long getMaxWithdrawal(final Credentials credentials) {
		return this.accountMapper.getMaxWithdrawal(credentials.getAccountNumber());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cpp.chalenge.johnson.atm.service.AccountService#getAccountBalance(com.cpp
	 * .chalenge.johnson.atm.model.Credentials)
	 */
	@Override
	@PinValidation
	public AccountBalance getAccountBalance(final Credentials credentials) {
		return new AccountBalance(getCurrentBalance(credentials), getMaxWithdrawal(credentials),
				credentials.getAccountNumber());
	}

}
