package com.cpp.chalenge.johnson.atm.exception;

/**
 * The Class IncorrectPinNumberException.
 */
public class IncorrectPinNumberException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5696529768261370571L;

}
