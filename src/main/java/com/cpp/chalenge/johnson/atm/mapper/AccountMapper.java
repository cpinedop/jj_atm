package com.cpp.chalenge.johnson.atm.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * The Interface AccountMapper.
 */
@Mapper
public interface AccountMapper {

	/**
	 * Checks if the account exists.
	 *
	 * @param accountNumber the account number
	 * @return true, if successful
	 */
	@Select("select count(id_account)>0 from t_accounts where account_number=#{accountNumber};")
	boolean doExist(@Param("accountNumber") Long accountNumber);

	/**
	 * Gets the account balance.
	 *
	 * @param accountNumber the account number
	 * @return the account balance
	 */
	@Select("select (select opening_balance from t_accounts where account_number=#{accountNumber}) - (select ifnull(sum(withdrawal_amount),0) from t_withdrawals where account_number=#{accountNumber});")
	Long getAccountBalance(@Param("accountNumber") Long accountNumber);

	/**
	 * Checks if the pin number is correct.
	 *
	 * @param accountNumber the account number
	 * @param pinHash       the pin hash
	 * @return the boolean
	 */
	@Select("select pin_hash=#{pinHash} from t_accounts where account_number=#{accountNumber}")
	Boolean isPinCorrect(@Param("accountNumber") Long accountNumber, @Param("pinHash") String pinHash);

	/**
	 * Gets the max withdrawal amount.
	 *
	 * @param accountNumber the account number
	 * @return the max withdrawal
	 */
	@Select("select (select opening_balance + overdraft from t_accounts where account_number=#{accountNumber}) - (select ifnull(sum(withdrawal_amount),0) from t_withdrawals where account_number=#{accountNumber});")
	Long getMaxWithdrawal(Long accountNumber);
}
