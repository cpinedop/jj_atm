package com.cpp.chalenge.johnson.atm.mapper;

import java.util.Set;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.cpp.chalenge.johnson.atm.model.Note;
import com.cpp.chalenge.johnson.atm.model.NoteType;

/**
 * The Interface NoteMapper.
 */
@Mapper
public interface NoteMapper {

	/**
	 * Gets the available notes.
	 *
	 * @return the available notes
	 */
	@Select("select id_note as id, note_type as type, note_amount as amount from t_notes;")
	@Results({ @Result(property = "id", column = "id"),
			@Result(property = "type", column = "type", one = @One(select = "getNoteTypeByType")) })
	Set<Note> getAvailableNotes();

	/**
	 * Gets the noteType object by type.
	 *
	 * @param idType the id type
	 * @return the note type by type
	 */
	@Select("select id_note_type as id, value from t_note_types where id_note_type=#{idType};")
	NoteType getNoteTypeByType(@Param("idType") Long idType);

	/**
	 * Update notes.
	 *
	 * @param noteType      the note type
	 * @param currentAmount the current amount
	 */
	@Update("update t_notes set note_amount = #{currentAmount} where note_type=#{noteType};")
	void updateNotes(@Param("noteType") Long noteType, @Param("currentAmount") Long currentAmount);
}
