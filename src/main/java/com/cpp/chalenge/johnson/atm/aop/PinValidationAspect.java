package com.cpp.chalenge.johnson.atm.aop;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cpp.chalenge.johnson.atm.annotation.Loggable;
import com.cpp.chalenge.johnson.atm.exception.IncorrectPinNumberException;
import com.cpp.chalenge.johnson.atm.exception.InvalidCredentialsExcption;
import com.cpp.chalenge.johnson.atm.mapper.AccountMapper;
import com.cpp.chalenge.johnson.atm.model.Credentials;
import com.cpp.chalenge.johnson.atm.service.UtilsService;

/**
 * The Class PinValidationAspect.
 *
 * Uses the credentials passed as parameter to the annotated method for validate
 * the pin number associated to the account.
 */
@Aspect
@Component
public class PinValidationAspect {

	/** The logger. */
	final Logger logger = LogManager.getLogger(PinValidationAspect.class);

	/** The account mapper. */
	@Autowired
	private AccountMapper accountMapper;

	/** The utils service. */
	@Autowired
	private UtilsService utilsService;

	/**
	 * Allows the method execution only if the pin is valid.
	 *
	 * @param joinPoint the join point
	 * @return the object
	 * @throws Throwable the throwable
	 */
	@Loggable
	@Around("@annotation(com.cpp.chalenge.johnson.atm.annotation.PinValidation)")
	public Object arround(final ProceedingJoinPoint joinPoint) throws Throwable {
		final Credentials credentials = getCredentialsParameter(joinPoint.getArgs());

		if (credentials == null || credentials.getAccountNumber() == null || credentials.getPinNumber() == null) {
			this.logger.error("Invalid credentials");
			throw new InvalidCredentialsExcption();
		}

		final String pinHash = this.utilsService.getSha256Hash(String.valueOf(credentials.getPinNumber()));

		if (pinHash != null && !this.accountMapper.isPinCorrect(credentials.getAccountNumber(), pinHash)) {
			this.logger.error("Incorrect pin number");
			throw new IncorrectPinNumberException();
		}

		return joinPoint.proceed();
	}

	/**
	 * Gets the credentials parameter.
	 *
	 * @param parameters the parameters
	 * @return the credentials parameter
	 */
	private Credentials getCredentialsParameter(final Object[] parameters) {
		for (final Object parameter : parameters) {
			if (parameter instanceof Credentials)
				return (Credentials) parameter;
		}

		return null;
	}
}
