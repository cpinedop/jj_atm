package com.cpp.chalenge.johnson.atm.application;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * The Class Application.
 */
@SpringBootApplication
@ComponentScan("com.cpp.chalenge.johnson.atm.application.config, com.cpp.chalenge.johnson.atm.controller, com.cpp.chalenge.johnson.atm.service.impl, com.cpp.chalenge.johnson.atm.aop, com.cpp.chalenge.johnson.atm.security")
@MapperScan("com.cpp.chalenge.johnson.atm.mapper")
public class Application {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(final String[] args) {
		final Logger logger = LogManager.getLogger(Application.class);

		logger.debug("Starting application");

		final SpringApplication app = new SpringApplication(Application.class);
		app.run(args);

		logger.debug("Application started");
	}
}
