package com.cpp.chalenge.johnson.atm.service.impl;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.cpp.chalenge.johnson.atm.service.UtilsService;

/**
 * The Class UtilsServiceImpl.
 */
@Service
public class UtilsServiceImpl implements UtilsService {

	/** The logger. */
	final Logger logger = LogManager.getLogger(UtilsServiceImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cpp.chalenge.johnson.atm.service.UtilsService#getCurrentAccount()
	 */
	@Override
	public Long getCurrentAccount() {
		final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return (Long) auth.getPrincipal();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cpp.chalenge.johnson.atm.service.UtilsService#getSha256Hash(java.lang.
	 * String)
	 */
	@Override
	public String getSha256Hash(final String source) {
		return DigestUtils.sha256Hex(source);
	}

}
