package com.cpp.chalenge.johnson.atm.controller;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cpp.chalenge.johnson.atm.annotation.Loggable;
import com.cpp.chalenge.johnson.atm.exception.IncorrectPinNumberException;
import com.cpp.chalenge.johnson.atm.model.CommonResponse;
import com.cpp.chalenge.johnson.atm.model.Credentials;
import com.cpp.chalenge.johnson.atm.model.WithdrawalOperation;
import com.cpp.chalenge.johnson.atm.service.AccountService;
import com.cpp.chalenge.johnson.atm.service.UtilsService;
import com.cpp.chalenge.johnson.atm.service.WithdrawalService;

/**
 * The Class WithdrawalController.
 */
@RestController
public class WithdrawalController {

	/** The logger. */
	final Logger logger = LogManager.getLogger(WithdrawalController.class);

	/** The utils service. */
	@Autowired
	private UtilsService utilsService;

	/** The account service. */
	@Autowired
	private AccountService accountService;

	/** The withdrawal service. */
	@Autowired
	private WithdrawalService withdrawalService;

	/**
	 * Request withdrawal.
	 *
	 * @param pinNumber           the pin number
	 * @param withdrawalRequested the withdrawal requested
	 * @return the response entity
	 */
	@GetMapping(value = "/requestWithdrawal", produces = MediaType.APPLICATION_JSON_VALUE)
	@Loggable
	public ResponseEntity<CommonResponse> requestWithdrawal(@PathParam("pinNumber") final Long pinNumber,
			@PathParam("withdrawalRequested") final Long withdrawalRequested) {
		final Long accountNumber = this.utilsService.getCurrentAccount();
		final Credentials credentials = new Credentials(accountNumber, pinNumber);

		final CommonResponse commonResponse = new CommonResponse();

		final Long maxWithdrawal = this.accountService.getMaxWithdrawal(credentials);
		if (maxWithdrawal >= withdrawalRequested) {
			final WithdrawalOperation withdrawalOperation = this.withdrawalService
					.requestWithdrawalOperation(credentials, withdrawalRequested);

			commonResponse.setSuccess(!withdrawalOperation.isInsufficientNotes());
			commonResponse.setData(withdrawalOperation);
		} else {
			this.logger.debug("Inssuficient founds");
			commonResponse.setSuccess(false);
			commonResponse.setData("Inssuficient founds the maximum withdrawal amount is " + maxWithdrawal);
		}

		return new ResponseEntity<>(commonResponse, HttpStatus.OK);
	}

	/**
	 * Handle error.
	 *
	 * @param req the req
	 * @param ex  the ex
	 * @return the response entity
	 */
	@ExceptionHandler(IncorrectPinNumberException.class)
	public ResponseEntity<CommonResponse> handleError(final HttpServletRequest req, final Exception ex) {
		this.logger.debug("Incorrect pin number");
		final CommonResponse commonResponse = new CommonResponse();
		commonResponse.setSuccess(false);
		commonResponse.setData("Incorrect pin number");

		return new ResponseEntity<>(commonResponse, HttpStatus.OK);
	}
}
