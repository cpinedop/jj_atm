package com.cpp.chalenge.johnson.atm.service;

/**
 * The Interface UtilsService.
 */
public interface UtilsService {

	/**
	 * Gets the current account.
	 *
	 * @return the current account
	 */
	Long getCurrentAccount();

	/**
	 * Gets the sha 256 hash.
	 *
	 * @param source the source
	 * @return the sha 256 hash
	 */
	String getSha256Hash(final String source);
}
