package com.cpp.chalenge.johnson.atm.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.cpp.chalenge.johnson.atm.mapper.AccountMapper;

/**
 * The Class SecureCardAuthFilter.
 *
 * This class handles the authentication process.
 */
@Component
public class SecureCardAuthFilter extends OncePerRequestFilter {

	/** The logger. */
	final Logger customFilterLogger = LogManager.getLogger(SecureCardAuthFilter.class);

	/** The account mapper. */
	@Autowired
	private AccountMapper accountMapper;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.springframework.web.filter.OncePerRequestFilter#doFilterInternal(javax.
	 * servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * javax.servlet.FilterChain)
	 */
	@Override
	protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws ServletException, IOException {
		final String xAuth = request.getHeader("accountNumber");

		if (!isValid(xAuth)) {
			filterChain.doFilter(request, response);
			return;
		}

		boolean error = true;
		Long accountNumber = null;
		try {
			accountNumber = getAccountNumberByToken(xAuth);
			error = false;
		} catch (final SecurityException e) {
			this.customFilterLogger.debug("Malformed account number");
		}

		if (error) {
			filterChain.doFilter(request, response);
			return;
		}

		final Authentication auth = new SecureCardTokenAuthentication(accountNumber);
		SecurityContextHolder.getContext().setAuthentication(auth);

		filterChain.doFilter(request, response);
	}

	/**
	 * Gets the account number by token.
	 *
	 * @param xAuth the auth header value
	 * @return the account number by token
	 */
	private Long getAccountNumberByToken(final String xAuth) {
		try {
			return Long.parseLong(xAuth);
		} catch (final NumberFormatException e) {
			throw new SecurityException();
		}
	}

	/**
	 * Checks if the authentication parameters are valid.
	 *
	 * @param xAuth the auth header value
	 * @return true, if the authentication parameters are valid.
	 */
	private boolean isValid(final String xAuth) {
		return xAuth != null && this.accountMapper.doExist(Long.parseLong(xAuth));
	}

}
