package com.cpp.chalenge.johnson.atm.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.cpp.chalenge.johnson.atm.annotation.Loggable;
import com.cpp.chalenge.johnson.atm.exception.IncorrectPinNumberException;
import com.cpp.chalenge.johnson.atm.model.AccountBalance;
import com.cpp.chalenge.johnson.atm.model.CommonResponse;
import com.cpp.chalenge.johnson.atm.model.Credentials;
import com.cpp.chalenge.johnson.atm.service.AccountService;
import com.cpp.chalenge.johnson.atm.service.UtilsService;

/**
 * The Class AccountController.
 */
@RestController
public class AccountController {

	/** The logger. */
	final Logger logger = LogManager.getLogger(AccountController.class);

	/** The utils service. */
	@Autowired
	private UtilsService utilsService;

	/** The account service. */
	@Autowired
	private AccountService accountService;

	/**
	 * Gets the current balance.
	 *
	 * @param pinNumber the pin number
	 * @return the current balance
	 */
	@GetMapping(value = "/getBalance/{pinNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
	@Loggable
	public ResponseEntity<CommonResponse> getCurrentBalance(@PathVariable final Long pinNumber) {
		final Long accountNumber = this.utilsService.getCurrentAccount();
		final Credentials credentials = new Credentials(accountNumber, pinNumber);

		final CommonResponse commonResponse = new CommonResponse();

		final AccountBalance balance = this.accountService.getAccountBalance(credentials);
		commonResponse.setSuccess(true);
		commonResponse.setData(balance);

		return new ResponseEntity<>(commonResponse, HttpStatus.OK);
	}

	/**
	 * Handle error.
	 *
	 * @param req the req
	 * @param ex  the ex
	 * @return the response entity
	 */
	@ExceptionHandler(IncorrectPinNumberException.class)
	public ResponseEntity<CommonResponse> handleError(final HttpServletRequest req, final Exception ex) {
		this.logger.debug("Incorrect pin number");
		final CommonResponse commonResponse = new CommonResponse();
		commonResponse.setSuccess(false);
		commonResponse.setData("Incorrect pin number");

		return new ResponseEntity<>(commonResponse, HttpStatus.OK);
	}
}
