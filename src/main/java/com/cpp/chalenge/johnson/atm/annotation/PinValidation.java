package com.cpp.chalenge.johnson.atm.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The Interface PinValidation.
 *
 * Annotation for validate the pin number it triggers the PinValidationAspect
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface PinValidation {

}
