--DROP TABLE IF EXISTS jjatm_database.t_test_data;

--CREATE TABLE jjatm_database.t_test_data(id_data INT NOT NULL AUTO_INCREMENT, data TEXT NULL, PRIMARY KEY (id_data));
--INSERT INTO jjatm_database.t_test_data (data) VALUES ('a');

SET NAMES utf8;

use jjatm_database;

-- Delete tables
DROP TABLE IF EXISTS t_withdrawals;
DROP TABLE IF EXISTS t_accounts;
DROP TABLE IF EXISTS jjatm_database.t_notes;
DROP TABLE IF EXISTS jjatm_database.t_note_types;

-- Create tables
CREATE TABLE jjatm_database.t_note_types (id_note_type INT NOT NULL AUTO_INCREMENT, value INT NOT NULL, PRIMARY KEY (id_note_type));
CREATE TABLE jjatm_database.t_notes (id_note INT NOT NULL AUTO_INCREMENT, note_type INT NOT NULL, note_amount INT NOT NULL, PRIMARY KEY (id_note), INDEX fk_note_note_types_idx (note_type ASC), CONSTRAINT fk_note_note_types FOREIGN KEY (note_type) REFERENCES jjatm_database.t_note_types (id_note_type) ON DELETE NO ACTION ON UPDATE NO ACTION);
CREATE TABLE t_accounts (id_account int(11) NOT NULL AUTO_INCREMENT, account_number bigint(9) NOT NULL, pin_hash char(64) NOT NULL COMMENT 'sha256 hash', opening_balance bigint(20) NOT NULL, overdraft bigint(20) NOT NULL, PRIMARY KEY (id_account), KEY account_number (account_number));
CREATE TABLE t_withdrawals (id_withdrawal int(11) NOT NULL AUTO_INCREMENT, account_number bigint(9) NOT NULL, withdrawal_amount bigint(20) NOT NULL, withdrawal_date timestamp NOT NULL DEFAULT current_timestamp(), PRIMARY KEY (id_withdrawal), KEY fk_withdrawals_accounts_idx (account_number), CONSTRAINT fk_withdrawals_accounts FOREIGN KEY (account_number) REFERENCES t_accounts (account_number) ON DELETE NO ACTION ON UPDATE NO ACTION);

-- Populate t_note_types
INSERT INTO jjatm_database.t_note_types (value) VALUES ('500');
INSERT INTO jjatm_database.t_note_types (value) VALUES ('1000');
INSERT INTO jjatm_database.t_note_types (value) VALUES ('2000');
INSERT INTO jjatm_database.t_note_types (value) VALUES ('5000');

-- Populate t_note
INSERT INTO jjatm_database.t_notes (note_type, note_amount) VALUES ('1', '40');
INSERT INTO jjatm_database.t_notes (note_type, note_amount) VALUES ('2', '20');
INSERT INTO jjatm_database.t_notes (note_type, note_amount) VALUES ('3', '30');
INSERT INTO jjatm_database.t_notes (note_type, note_amount) VALUES ('4', '20');

-- Populate t_accounts
INSERT INTO jjatm_database.t_accounts (account_number, pin_hash, opening_balance, overdraft) VALUES ('123456789', '03AC674216F3E15C761EE1A5E255F067953623C8B388B4459E13F978D7C846F4', '80000', '20000');
INSERT INTO jjatm_database.t_accounts (account_number, pin_hash, opening_balance, overdraft) VALUES ('987654321', 'FE2592B42A727E977F055947385B709CC82B16B9A87F88C6ABF3900D65D0CDC3', '123000', '15000');

-- Populate t_withdrawals
INSERT INTO jjatm_database.t_withdrawals (id_withdrawal, account_number, withdrawal_amount) VALUES ('0', '123456789', '4000');
INSERT INTO jjatm_database.t_withdrawals (id_withdrawal, account_number, withdrawal_amount) VALUES ('0', '123456789', '2000');

