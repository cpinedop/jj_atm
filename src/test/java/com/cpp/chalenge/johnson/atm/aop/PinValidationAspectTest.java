package com.cpp.chalenge.johnson.atm.aop;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.cpp.chalenge.johnson.atm.exception.IncorrectPinNumberException;
import com.cpp.chalenge.johnson.atm.exception.InvalidCredentialsExcption;
import com.cpp.chalenge.johnson.atm.mapper.AccountMapper;
import com.cpp.chalenge.johnson.atm.model.Credentials;
import com.cpp.chalenge.johnson.atm.service.UtilsService;

/**
 * The Class PinValidationAspectTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class PinValidationAspectTest {

	/** The logger. */
	final Logger logger = LogManager.getLogger(PinValidationAspect.class);

	/** The service. */
	@InjectMocks
	private final PinValidationAspect service = new PinValidationAspect();

	/** The account mapper. */
	@Mock
	private AccountMapper accountMapper;

	/** The utils service. */
	@Mock
	private UtilsService utilsService;

	/** The join point. */
	@Mock
	ProceedingJoinPoint joinPoint;

	/**
	 * Inits the.
	 */
	@Before
	public void init() {
		Mockito.when(this.utilsService.getSha256Hash("1234")).thenReturn("abcd");
		Mockito.when(this.utilsService.getSha256Hash("1235")).thenReturn("");
		Mockito.when(this.accountMapper.isPinCorrect(this.account, "abcd")).thenReturn(true);
		Mockito.when(this.accountMapper.isPinCorrect(this.account, "")).thenReturn(false);
		try {
			Mockito.when(this.joinPoint.proceed()).thenReturn(200L);
		} catch (final Throwable e) {
			this.logger.error("Error creating mocks");
			fail();
		}

		this.okCredentials = new Credentials(this.account, this.pinNumberOk);
		this.koCredentials = new Credentials(this.account, this.pinNumberKo);
	}

	/** The account. */
	final Long account = 123456789L;

	/** The pin number ok. */
	Long pinNumberOk = 1234L;

	/** The pin number ko. */
	Long pinNumberKo = 1235L;

	/** The ok credentials. */
	Credentials okCredentials;

	/** The ko credentials. */
	Credentials koCredentials;

	/**
	 * Arround ok test.
	 */
	@Test
	public void arroundOkTest() {
		final Long expected = 200L;
		final Object[] params = { this.okCredentials };
		Mockito.when(this.joinPoint.getArgs()).thenReturn(params);

		Long result = null;
		try {
			result = (Long) this.service.arround(this.joinPoint);
		} catch (final Throwable e) {
			fail();
		}

		assertEquals(result, expected);
	}

	/**
	 * Arround ko bad ping test.
	 *
	 * @throws Throwable the throwable
	 */
	@Test(expected = IncorrectPinNumberException.class)
	public void arroundKoBadPingTest() throws Throwable {
		final Object[] params = { this.koCredentials };
		Mockito.when(this.joinPoint.getArgs()).thenReturn(params);

		this.service.arround(this.joinPoint);

	}

	/**
	 * Arround ko no credentials test.
	 *
	 * @throws Throwable the throwable
	 */
	@Test(expected = InvalidCredentialsExcption.class)
	public void arroundKoNoCredentialsTest() throws Throwable {
		final Object[] params = {};
		Mockito.when(this.joinPoint.getArgs()).thenReturn(params);

		this.service.arround(this.joinPoint);
	}

	/**
	 * Arround ko null credentials test.
	 *
	 * @throws Throwable the throwable
	 */
	@Test(expected = InvalidCredentialsExcption.class)
	public void arroundKoNullCredentialsTest() throws Throwable {
		final Credentials nullCredentials = null;
		final Object[] params = { nullCredentials };
		Mockito.when(this.joinPoint.getArgs()).thenReturn(params);

		this.service.arround(this.joinPoint);
	}

}
