package com.cpp.chalenge.johnson.atm.service.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.cpp.chalenge.johnson.atm.mapper.AccountMapper;
import com.cpp.chalenge.johnson.atm.model.Credentials;

/**
 * The Class AccountServiceImplTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class AccountServiceImplTest {

	/** The service. */
	@InjectMocks
	private final AccountServiceImpl service = new AccountServiceImpl();

	/** The account mapper. */
	@Mock
	private AccountMapper accountMapper;

	/** The account. */
	final Long account = 123456789L;

	/** The pin number ok. */
	Long pinNumberOk = 1234L;

	/** The pin number ko. */
	Long pinNumberKo = 1235L;

	/** The ok credentials. */
	Credentials okCredentials;

	/** The ko credentials. */
	Credentials koCredentials;

	/**
	 * Inits the.
	 */
	@Before
	public void init() {
		Mockito.when(this.accountMapper.getAccountBalance(this.account)).thenReturn(200L);
		Mockito.when(this.accountMapper.getMaxWithdrawal(this.account)).thenReturn(200L);

		this.okCredentials = new Credentials(this.account, this.pinNumberOk);
	}

	/**
	 * Gets the current balance OK test.
	 *
	 * @return the current balance OK test
	 */
	@Test
	public void getCurrentBalanceOKTest() {
		final Long expected = 200L;

		final Long result = this.service.getCurrentBalance(this.okCredentials);

		assertEquals(result, expected);
	}

	/**
	 * Gets the max withdrawal ok test.
	 *
	 * @return the max withdrawal ok test
	 */
	@Test
	public void getMaxWithdrawalOkTest() {
		final Long expected = 200L;

		final Long result = this.service.getMaxWithdrawal(this.okCredentials);

		assertEquals(result, expected);
	}

}
