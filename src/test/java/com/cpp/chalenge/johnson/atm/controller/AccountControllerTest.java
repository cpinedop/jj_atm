package com.cpp.chalenge.johnson.atm.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.cpp.chalenge.johnson.atm.model.AccountBalance;
import com.cpp.chalenge.johnson.atm.model.CommonResponse;
import com.cpp.chalenge.johnson.atm.model.Credentials;
import com.cpp.chalenge.johnson.atm.service.AccountService;
import com.cpp.chalenge.johnson.atm.service.UtilsService;

/**
 * The Class AccountControllerTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class AccountControllerTest {

	/** The controller. */
	@InjectMocks
	private final AccountController controller = new AccountController();

	/** The account service. */
	@Mock
	private AccountService accountService;

	/** The utils service. */
	@Mock
	private UtilsService utilsService;

	/**
	 * Inits the.
	 */
	@Before
	public void init() {
		Mockito.when(this.utilsService.getCurrentAccount()).thenReturn(123456789L);
	}

	/**
	 * Gets the current balance OK test.
	 *
	 * @return the current balance OK test
	 */
	@Test
	public void getCurrentBalanceOKTest() {
		final AccountBalance balance = new AccountBalance(200L, 200L, 123456789L);

		Mockito.when(this.accountService.getCurrentBalance(Mockito.any(Credentials.class))).thenReturn(200L);
		Mockito.when(this.accountService.getMaxWithdrawal(Mockito.any(Credentials.class))).thenReturn(200L);
		Mockito.when(this.accountService.getAccountBalance(Mockito.any(Credentials.class))).thenReturn(balance);

		final CommonResponse cr = new CommonResponse();
		cr.setSuccess(true);
		cr.setData(balance);
		final ResponseEntity<CommonResponse> expected = new ResponseEntity<>(cr, HttpStatus.OK);

		final ResponseEntity<CommonResponse> result = this.controller.getCurrentBalance(1234L);

		assertEquals(expected.getBody(), result.getBody());
		assertEquals(expected.getStatusCode(), result.getStatusCode());
	}

}
